import logger from "../logger"

/**
 * @param attachments the attachments with the message
 * @return {boolean|*}
 */
export function isPGEncrypted(attachments) {
    const filtered = attachments.filter((att) => att.fileName === 'postguard.encrypted')
    return filtered.length === 1
}

/**
 * @param {Text} message the message
 * @return {boolean|*}
 */
export const isPGMessage = (message) =>
	message.format === 'plain' && message.value.startsWith('-----BEGIN POSTGUARD MESSAGE-----')